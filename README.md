
## Introduction

Allowing users to log in to your app is one of the most common features you’ll add to your web application. This article will cover how to add authentication to your Flask app with the Flask-Login package.

## What do you get from this repo?

- Use the Flask-Login library for session management
- Use the built-in Flask utility for hashing passwords
- Add protected pages to our app for logged in users only
- Use Flask-SQLAlchemy to create a user model
- Create sign up and login forms for our users to create accounts and log in
- Flash error messages back to users when something goes wrong
- Use information from the user’s account to display on the profile page


## Prerequisites

- Python installed on a local environment.
- Knowledge of Basic Linux Navigation and File Management is helpful but not required.
- Familiarity with an editor like Visual Studio Code is helpful but not required.